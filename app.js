var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var pjson = require('./package.json');
var port = process.env.PORT || 3000;
switch( process.env.ENV_1 )
{
  case "jenkins":
    port = 3001;
    break;
}

var version = pjson.version;
var name = pjson.name;

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

app.get('/test', function(req, res){
  io.emit('chat message', 'Test message received');
  res.send('OK');
});

io.on('connection', function(socket){
  socket.on('chat message', function(msg){
    io.emit('chat message', msg);
    if (msg.match(/version/i)) {
      io.emit('chat message', ' Welcome!' + ' I am running ' + name + ' version ' + version);
    } else if ((msg.match(/hi/i) || msg.match(/hello/i)) && version >= '2.0.0') {
      io.emit('chat message', 'Hello!');
    } else if (msg.match(/hola/i) && version == '2.0.1') {
      io.emit('chat message', 'Hola!');
    }
  });
});

http.listen(port, function(){
  
  console.log('listening on *:' + port);
});

module.exports = app;
